import copy
import string
import warnings

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import pandas as pd
import pyrcmip.assessed_ranges
import scmdata
import seaborn as sns
import tqdm.autonotebook as tqdman
from matplotlib.gridspec import GridSpec

from .rcmip import add_oscar_reported_results


SAVE_KWARGS = {
    "bbox_inches": "tight",
    "pad_inches": 0.1,
    "transparent": True,
}


ANNOTATION_PROPS = {}


CLIMATE_MODEL_PALETTE = {
    "assessed range": "black",
    "Proxy assessment": "black",
    "HadCRUT.4.6.0.0": "black",
    "von Schuckmann et al., 2020": "black",
#     "MAGICC7": "tab:red",
    "MAGICCv7.5.1": "tab:red",
    "MCE-v1-2": "tab:green",
    "Cicero-SCM": "tab:orange",
    "FaIRv2.0.0-alpha": "tab:olive",
    "FaIR1.6": "tab:purple",
    "OSCARv3.1": "tab:cyan",
    "Hector": "tab:brown",
#     "SCM4OPTv2.0": "tab:pink",
    "SCM4OPTv2.1": "tab:pink",
    "EMGC": "tab:gray",
}


def setup():
    import matplotlib
#     import seaborn as sns

    figsize = (7, 6)
#     sns.set(rc={"figure.figsize": figsize})
    matplotlib.rcParams["figure.figsize"] = figsize

#     # thank you http://jonathansoma.com/lede/data-studio/matplotlib/exporting-from-matplotlib-to-open-in-adobe-illustrator/
#     matplotlib.rcParams["pdf.fonttype"] = 42  # avoid Type 3 fonts
#     matplotlib.rcParams["ps.fonttype"] = 42  # avoid Type 3 fonts

    # output text properly thank you https://stackoverflow.com/a/50563208
    matplotlib.rcParams["pdf.use14corefonts"] = True
#     matplotlib.rcParams["text.usetex"] = False
    
    # thank you https://stackoverflow.com/q/15538099
    matplotlib.rcParams['axes.unicode_minus']=False

    # set font
    matplotlib.rcParams["font.family"] = "DejaVu Sans"
    matplotlib.rcParams["font.sans-serif"] = ["Helvetica"]


#     matplotlib.rcParams["lines.linewidth"] = 2
#     matplotlib.rcParams["axes.facecolor"] = "white"
#     matplotlib.rcParams["axes.edgecolor"] = "black"
#     matplotlib.rcParams["xtick.bottom"] = True
#     matplotlib.rcParams["ytick.left"] = True
#     # make sure grid lines appear if they're on
#     matplotlib.rcParams["grid.color"] = "black"
#     matplotlib.rcParams["axes.grid"] = False


    matplotlib.rcParams["font.size"] = 10

    matplotlib.rcParams["axes.labelsize"] = 10
#     matplotlib.rcParams["axes.labelweight"] = "bold"

    matplotlib.rcParams["xtick.labelsize"] = 10
    matplotlib.rcParams["ytick.labelsize"] = 10

#     matplotlib.rcParams["axes.titlesize"] = 16
#     matplotlib.rcParams["figure.titleweight"] = "bold"

    matplotlib.rcParams["legend.fontsize"] = 10
    matplotlib.rcParams["legend.handlelength"] = 2


def calculate_quantiles(
    scmrun,
    quantiles,
    process_over_columns=("run_id", "ensemble_member", "climate_model"),
):
    """
    Calculate quantiles of an :obj:`ScmRun`
    
    Parameters
    ----------
    scmrun : :obj:`ScmRun`
        :obj:`ScmRun` containing the data from which to calculate the
        quantiles
    quantiles : list of float
        quantiles to calculate (must be in [0, 1])
    process_over_columns : list of str
        Columns to process over. All other columns in ``scmdf.meta`` will be
        included in the output.
    Returns
    -------
    :obj:`ScmRun`
        :obj:`ScmRun` containing the quantiles of interest, processed
        over ``process_over_columns``
    """
    out = []
    for quant in quantiles:
        quantile_df = scmrun.process_over(process_over_columns, "quantile", q=quant)
        quantile_df["quantile"] = quant

        out.append(scmdata.ScmRun(quantile_df))

    out = scmdata.run_append(out)

    return out


def make_plume_plot(
    scmrun,
    ax,
    hue_palette=None,
    hue_var="scenario",
    hue_label="Scenario",
    style_dict=None,
    style_var="climate_model",
    style_label="Climate model",
    quantiles_plumes=[((0.05, 0.95), 0.5), ((0.5,), 1.0),],
    linewidth=1,
):
    """
    scmrun - ScmRun
    ax - axes to plot on
    
    hue_palette - palette to use for the hue variable
    hue_var - variable to use to determine hue
    hue_label - label of the hue in plots
    
    quantiles_plumes - list of tuples. The first element of each tuple is 
        also a tuple which defines the quantiles. The second element is
        a float which defines the alpha with which to plot.
        
    linewidth - linewidth for lines
    
    style_dict - dictionary defining which style to use for each value in style_var
    style_var - the variable which defines what style to use (in scmrum)
    style_label - the text to use in the legend
    """
    quantile_labels = {}
    for q, alpha in quantiles_plumes:
        for hdf in scmrun.groupby(hue_var):
            hue_value = hdf.get_unique_meta(hue_var, no_duplicates=True)
            pkwargs = {"color": hue_palette[hue_value]}

            for hsdf in hdf.groupby(style_var):
                style_value = hsdf.get_unique_meta(style_var, no_duplicates=True)

                quantiles = calculate_quantiles(hsdf, q, ("ensemble_member",))
                if len(q) == 2:
                    label = "{:.0f}th - {:.0f}th".format(q[0] * 100, q[1] * 100)
                    p = ax.fill_between(
                        quantiles["year"],
                        quantiles.values[0],
                        quantiles.values[1],
                        alpha=alpha,
                        label=label,
                        **pkwargs
                    )
                else:
                    label = "{:.0f}th".format(q[0] * 100)
                    p = ax.plot(
                        quantiles["year"],
                        quantiles.values[0],
                        label=label,
                        linewidth=linewidth,
                        linestyle=style_dict[style_value],
                        **pkwargs
                    )[0]

                quantile_labels[label] = p

    # Fake the line handles for the legend
    hue_val_lines = [
        mlines.Line2D([0], [0], **{"color": hue_palette[hue_value]}, label=hue_value)
        for hue_value in scmrun.get_unique_meta(hue_var)
    ]

    style_val_lines = [
        mlines.Line2D(
            [0], [0], **{"linestyle": style_dict[style_value]}, label=style_value
        )
        for style_value in scmrun.get_unique_meta(style_var)
    ]

    legend_items = [
        mpatches.Patch(alpha=0, label="Quantiles"),
        *quantile_labels.values(),
        mpatches.Patch(alpha=0, label=hue_label),
        *hue_val_lines,
        mpatches.Patch(alpha=0, label=style_label),
        *style_val_lines,
    ]

    ax.legend(handles=legend_items, loc="upper left")

    return ax, legend_items


def get_calc_df(inp, v, s):
    return inp.filter(variable=v, scenario=s)


def get_hue_order(model_dat, obs=None, cmip6=False, cmip6_input=None):
    rcms = sorted(
        model_dat.get_unique_meta("climate_model"), key=str.casefold
    )
    
    if cmip6_input is not None:
        models = ["CMIP6 ScenarioMIP inputs"] + rcms
    else:
        models = rcms
        
    if cmip6:
        models = ["CMIP6 MME"] + models
    else:
        models = models
        
    if obs is not None:
        return obs.get_unique_meta("source") + models
    
    return models


def get_sizes(hue_order, linewidth, linewidth_cmip6):
    out = {}
    for h in hue_order:
        if h == "CMIP6 MME":
            out[h] = linewidth_cmip6
        else:
            out[h] = linewidth
            
    return out


def make_ts_plot(
    ax,
    inp,
    ref_period,
    quantiles,
    plt_years,
    palette,
    hue,
    hue_order,
    linewidth=3,
    alpha=0.7,
    process_over_columns=("ensemble_member",),
    set_ylabel=True,
    legend=True,
    style=None,
    dashes=None,
    size=None,
    sizes=None,
    units=None,
):
    if ref_period is not None:
        inp = inp.relative_to_ref_period_mean(year=ref_period)

    qdf = calculate_quantiles(
        inp, quantiles, process_over_columns
    ).filter(year=plt_years)

    ax = qdf.lineplot(
        hue=hue,
        palette=palette,
        hue_order=hue_order,
        style=style,
        ax=ax,
        linewidth=linewidth,
        alpha=alpha,
        legend=legend,
        time_axis="year",
        dashes=dashes,
        size=size,
        sizes=sizes,
        units=units,
        estimator=None,
    )
    ax.set_xlim(plt_years[0], plt_years[-1])
    if set_ylabel:
        ylabel = get_ylabel(
            qdf.get_unique_meta("variable", no_duplicates=True),
            qdf.get_unique_meta("unit", no_duplicates=True),
            ref_period
        )

        ax.set_ylabel(ylabel)

    return ax


def get_ylabel(variable, unit, ref_period=None, evaluation_period=None):
    if evaluation_period is not None:
        if evaluation_period[0] == evaluation_period[-1]:
            variable = f"{evaluation_period[0]} {variable}"
            
        else:
            variable = f"{evaluation_period[0]}-{evaluation_period[-1]} {variable}"
        
    if ref_period is not None:
        if ref_period[0] == ref_period[-1]:
            ylabel = f"{variable}\nrel. to {ref_period[0]} ({unit})"

        else:
            ylabel = f"{variable}\nrel. to {ref_period[0]}-{ref_period[-1]} ({unit})"
    else:
        ylabel = f"{variable} ({unit})"
        
    return ylabel


def get_hacked_hists(df_in, metric_name, bins, custom_heights, include_scenario=False):
    if custom_heights is None:
        custom_heights = {}
        
    hacked_hists = []
    
    grouper = ["Source", "scenario"] if include_scenario else "Source"
    for label, df in tqdman.tqdm(df_in.groupby(grouper), leave=False, desc="Renormalising histograms"):
        if include_scenario:
            (source, scenario) = label
        else:
            source = label

        hist, _ = np.histogram(df[metric_name], bins)
        hist = hist.astype(float) / hist.sum()
        if source in custom_heights:
            hist *= custom_heights[source]

        new_df = []
        
        for count, lower, upper in zip(
            hist, bins[:-1], bins[1:]
        ):
            middle = (lower + upper) / 2
            tmp = {"x": middle, "y": count, "Source": source, "unit": df["unit"].unique()[0]}
            
            if include_scenario:
                tmp["scenario"] = scenario
                
            new_df.append(tmp)

        new_df = pd.DataFrame(new_df)
        hacked_hists.append(new_df)

    return pd.concat(hacked_hists)


def make_pdf_plot(ax, df, metric, palette, bins, custom_heights=None, hue="Source", legend=False, linewidth=2, vert=True, style=None, dashes=None, ymax_min=0):
    hide_poly_bottom_level = 0
    
    df_hacked = get_hacked_hists(df, metric, bins, custom_heights, include_scenario=style is not None)

    skwargs = dict(
        linewidth=linewidth,
        hue=hue,
        palette=palette,
        ax=ax,
        legend=legend,
        sort=False,
        estimator=None,
    )
        
    if style is not None:
        skwargs["style"] = style
        skwargs["dashes"] = dashes
        
    if vert:
        skwargs["x"] = "y"
        skwargs["y"] = "x"

    else:
        skwargs["x"] = "x"
        skwargs["y"] = "y"
        
    df_hacked = df_hacked.sort_values(by=["Source", "x"])
    
    ax = sns.lineplot(data=df_hacked, **skwargs)
    
    prob_label = "Relative probability"
    if vert:
        ax.set_xticklabels([])
        ax.set_xlabel(prob_label)
        ax.set_ylabel(metric)
        ax.set_xlim(xmin=hide_poly_bottom_level, xmax=max(ymax_min, df_hacked["y"].max() * 1.1))
    else:
        ax.set_yticklabels([])
        ax.set_ylabel(prob_label)
        ax.set_xlabel(metric)
        ax.set_ylim(ymin=hide_poly_bottom_level, ymax=max(ymax_min, df_hacked["y"].max() * 1.1))
        
    return ax


def make_box_plot(ax, box_plot_stats, palette, vert=True):            
    box_plot_labels_order_original = {
        v["label"]: i for i, v in enumerate(box_plot_stats)
    }

    box_plot_labels_order = sorted(
        box_plot_labels_order_original.keys(), key=str.casefold
    )
    if not vert:
        box_plot_labels_order = box_plot_labels_order[::-1]
        
    if "Proxy assessment" in box_plot_labels_order:
        box_plot_labels_order = ["Proxy assessment"] + [
            v for v in box_plot_labels_order if v != "Proxy assessment"
        ]

    box_plot_stats_sorted = [
        box_plot_stats[box_plot_labels_order_original[label]]
        for label in box_plot_labels_order
    ]
    box_out = ax.bxp(
        box_plot_stats_sorted, 
        showfliers=False, 
        vert=vert, 
        patch_artist=True, 
#         shownotches=True, 
        medianprops=dict(color="white")
    )
    
    box_plot_labels_order = [
        v for v in box_plot_labels_order
    ]
    if vert:
        ax.set_xticklabels(box_plot_labels_order, rotation=90)
    else:
        ax.set_yticklabels(box_plot_labels_order, rotation=0)

    for patch, median_line, label in zip(box_out["boxes"], box_out["medians"], box_plot_labels_order):
        # avoid disappearing medians if we have a very narrow distribution
        duplicate_vertices = 0
        patch_vertices = patch.get_path().vertices
        for i, v in enumerate(patch_vertices):
            close_count = 0
            for ov in patch_vertices[i + 1 :]:
                if np.isclose(v[0], ov[0]) and np.isclose(v[1], ov[1], rtol=1e-2):
                    close_count += 1
            
            if close_count > 0:
                duplicate_vertices += 1
                
        if duplicate_vertices > 1:
            median_line.set_color("black")

        if label == "Proxy assessment":
            label = "assessed range"
        patch.set_facecolor(palette[label])

    return ax


def get_box_plot_df(assessed_ranges, metric, model_results, include_assessed_pdf):
    box_plot_df = assessed_ranges._get_box_plot_df(metric, model_results)

    if not include_assessed_pdf:
        box_plot_df = box_plot_df.loc[box_plot_df["Source"] != "assessed range", :]
    else:
        box_plot_df["Source"] = box_plot_df["Source"].apply(lambda x: x.replace("assessed range", "Proxy assessment"))
        
    return box_plot_df


def make_hist_assessed_range_comparison_figure(
    assessed_ranges,
    data_scmrun,
    obs,
    variable,
    metric,
    include_assessed_pdf,
    quantiles,
    process_over_columns,
    linewidth,
    palette,
    hue,
    scenario,
    plt_years,
    long_ts_ref_periods_axes_plt_years,
    ax_ts,
    ax_pdf,
    ax_box,
    ylim_pdf,
    bins,
    custom_heights=None,
):
    calc_df = get_calc_df(data_scmrun, variable, scenario)

    hue_order = get_hue_order(calc_df, obs)
    model_results = assessed_ranges.calculate_metric_from_results(metric, calc_df)

    box_plot_df = get_box_plot_df(
        assessed_ranges, metric, model_results, include_assessed_pdf
    )

    box_plot_stats = assessed_ranges._get_box_whisker_stats_custom_quantiles(
        box_plot_df, metric, box_quantiles=(17, 83), whisker_quantiles=(5, 95)
    )
    
    if obs is not None:
        sdf = scmdata.run_append([calc_df, obs])
    else:
        sdf = calc_df.copy()
        
    sdf = sdf.time_mean("AC")
    sdf["Source"] = sdf["climate_model"]
    
    metric_ref_period, metric_evaluation_period = assessed_ranges.get_norm_period_evaluation_period(
        metric
    )
    ax_ts = make_ts_plot(
        ax_ts,
        sdf,
        metric_ref_period,
        quantiles,
        plt_years,
        palette,
        hue,
        hue_order,
        linewidth=linewidth,
        alpha=0.7,
        process_over_columns=process_over_columns,
    )

    ax_pdf = make_pdf_plot(
        ax_pdf,
        box_plot_df,
        metric,
        palette,
        bins=bins,
        hue="Source",
        legend=False,
        linewidth=2,
        custom_heights=custom_heights,
    )
    ax_pdf.set_ylim(ylim_pdf)
    pdf_ylabel = get_ylabel(
        assessed_ranges.get_col_for_metric(metric, "RCMIP variable"),
        assessed_ranges.get_col_for_metric(metric, "unit"),
        metric_ref_period,
        metric_evaluation_period,
    )
    ax_pdf.set_ylabel(pdf_ylabel)

    ax_box = make_box_plot(
        ax_box, 
        box_plot_stats, 
        palette, 
    )
    
    for i, (ax, ref_period, plt_years) in enumerate(long_ts_ref_periods_axes_plt_years):
        ax = make_ts_plot(
            ax,
            sdf,
            ref_period,
            quantiles,
            plt_years,
            palette,
            hue,
            hue_order,
            linewidth=linewidth,
            alpha=0.7,
            process_over_columns=process_over_columns,
            legend=False,
        )
        add_panel_label(ax, i)
        ax.set_xlabel("Year")
    
    add_panel_label(ax_ts, i + 1)
    add_panel_label(ax_pdf, i + 2)
    add_panel_label(ax_box, i + 3)
    
    ax_ts.set_xlabel("Year")

    return ax_ts, ax_pdf, ax_box, tuple([v[0] for v in long_ts_ref_periods_axes_plt_years])


def calculate_custom_boxplot_ars_and_name_from_results(
    res,
    variable,
    unit,
    scenarios,
    eval_period,
    ref_period,
    eval_method="mean",
    region="World",
    include_assessed_pdf=False,
    include_oscar_custom=True,
):
    if ref_period is None:
        ref_period = [np.nan, np.nan]
        ref_period_str = ""
    else:
        ref_period_str = f" rel. to {ref_period[0]}-{ref_period[-1]}"

    if eval_period in ["peak", "peak_time", "peak_time_21yr_smooth"]:
        if eval_period == "peak":
            peak_str = f"Peak {variable}"
        else:
            peak_str = f"Year of peak {variable}"

        if ref_period_str:
            name = f"{peak_str}\n{ref_period_str.strip()} ({unit})"
        else:
            name = f"{peak_str} ({unit})"

        evaluation_period = [np.nan, np.nan]
    else:
        name = (
            f"{eval_period[0]}-{eval_period[-1]} {variable}\n{ref_period_str} ({unit})"
        )
        evaluation_period = eval_period
    
    box_plot_df = []
    for scenario in scenarios:
        print(scenario)
        ar = pyrcmip.assessed_ranges.AssessedRanges(
            pd.DataFrame(
                [
                    {
                        "RCMIP name": name,
                        "RCMIP variable": variable,
                        "RCMIP region": region,
                        "RCMIP scenario": scenario,
                        "evaluation_period_start": evaluation_period[0],
                        "evaluation_period_end": evaluation_period[-1],
                        "norm_period_start": ref_period[0],
                        "norm_period_end": ref_period[-1],
                        "very_likely__lower": 0.5,
                        "likely__lower": 1.0,
                        "central": 1.5,
                        "likely__upper": 2.0,
                        "very_likely__upper": 2.5,
                        "unit": unit,
                        "RCMIP evaluation method": eval_method,
                        "Source": "not used",
                    }
                ]
            )
        )

        if eval_period in ["peak", "peak_time", "peak_time_21yr_smooth"]:
            model_results = res.filter(variable=variable, scenario=scenario).convert_unit(
                unit
            )
            if not np.isnan(ref_period[0]):
                model_results = model_results.relative_to_ref_period_mean(year=ref_period)

            if eval_period == "peak":
                model_results = model_results.timeseries().max(axis="columns")
            elif eval_period == "peak_time":
                model_results = model_results.timeseries(time_axis="year").idxmax(
                    axis="columns"
                )
            elif eval_period == "peak_time_21yr_smooth":
                model_results = (
                    model_results.timeseries(time_axis="year")
                    .rolling(21, axis="columns", center=True)
                    .mean()
                    .idxmax(axis="columns")
                )
            else:
                raise NotImplementedError(eval_period)

            model_results.name = "value"
            model_results = model_results.to_frame().reset_index()
            model_results[ar.metric_column] = name
        else:
            model_results = ar.calculate_metric_from_results(name, res)
        
        if include_oscar_custom:
            oscar_metric = "{} {}".format(scenario, name.replace('\n', ' ').replace("  ", " ").strip())
            print(oscar_metric)
            model_results = add_oscar_reported_results(
                model_results,
                oscar_metric,
                overwrite_oscar_metric_name=True,
            )

        box_plot_df_scenario = (
            get_box_plot_df(
                ar, name, model_results, include_assessed_pdf=include_assessed_pdf
            )
        )
        box_plot_df_scenario["scenario"] = scenario
        box_plot_df.append(box_plot_df_scenario)
    
    box_plot_df = pd.concat(box_plot_df)
    
    return (
        box_plot_df,
        ar,
        name,
    )
    
    
def get_box_whisker_stats_custom_quantiles_multi_scenario(
    custom_ars,
    box_plot_df_in,
    metric_name,
    box_quantiles,
    whisker_quantiles,
):
    out = []
    for scenario, scen_df in box_plot_df_in.groupby("scenario"):
        box_plot_scen = custom_ars._get_box_whisker_stats_custom_quantiles(
            scen_df,
            metric_name,
            box_quantiles=box_quantiles,
            whisker_quantiles=whisker_quantiles,
        )

        if len(box_plot_df_in["scenario"].unique()) > 1:
            for v in box_plot_scen:
                v["label"] = "{} {}".format(v["label"], scenario)

        out += box_plot_scen

    return out


def get_longest_ensemble_member_for_climate_model(idf, years, years_no_nan):
    """
    Get longest ensemble ensemble member for each model for CMIP
    """
    db_cmip = []
    for sdf in idf.filter(year=years).groupby(["scenario", "climate_model"]):
        if years_no_nan is not None:
            sdf_ts_no_nan = sdf.filter(year=years_no_nan).timeseries().dropna()
            if sdf_ts_no_nan.empty:
                print(
                    "No useable data for {} {}".format(
                        sdf.get_unique_meta("climate_model", True),
                        sdf.get_unique_meta("scenario", True),
                    )
                )
                continue
        else:
            # don't discriminate on whether there are nan values or not
            sdf_ts_no_nan = sdf.timeseries()

        sdf_ts = sdf.timeseries().loc[sdf_ts_no_nan.index, :]
        nan_times = sdf_ts.isnull().sum(axis="columns")
        longest_runs = scmdata.ScmRun(sdf_ts.loc[nan_times == nan_times.min(), :])

        member_ids = longest_runs["member_id"].unique()

        # get the shortest strings first then sort
        # this ensures that we choose e.g. r1i1ip1f1 over r10i1p1f1
        member_ids_min_length = min([len(m) for m in member_ids])
        member_id_to_keep = min([m for m in member_ids if len(m) == member_ids_min_length])
        db_cmip.append(longest_runs.filter(member_id=member_id_to_keep))

    db_cmip = scmdata.run_append(db_cmip)
    
    return db_cmip


def make_overview_plot(
    data_scmrun,
    obs,
    quantiles,
    process_over_columns,
    scenario,
    ref_period,
    eval_period_end,
    plt_years,
    ylim_ts,
    linewidth,
    palette,
    hue,
    units,
    variable,
    cmip_var,
    unit,
    include_peak,
    custom_heights,
    cmip6=False,
    cmip_timeseries=None,
    include_assessed_pdf=False,
    scenario_esm=None,
    bin_width=0.1,
    cmip6_input=None,
    include_oscar_custom=True,
    force_fig_width=None
):
    scenario_list = [scenario] if scenario_esm is None else [scenario, scenario_esm]
    
    if include_peak:
        nrows = 3
        height_ratios = [2, 1, 1]
        if scenario_esm is not None:
            figsize = (14, 12)
        else:
            figsize = (12, 12)
    else:
        nrows = 1
        height_ratios = [1]
        if scenario_esm is not None:
            figsize = (14, 6)
        else:
            figsize = (12, 6)
    
    if force_fig_width is not None:
        figsize = (force_fig_width, figsize[1])

    fig = plt.figure(figsize=figsize)
    gs = GridSpec(
        nrows,
        3,
        width_ratios=[2, 1, 1],
        height_ratios=height_ratios,
        wspace=0.4,
        hspace=0.2,
    )

    ax_ts = fig.add_subplot(gs[0, 0])
    ax_end_period_pdf = fig.add_subplot(gs[0, 1], sharey=ax_ts)
    ax_end_period_box = fig.add_subplot(gs[0, 2], sharey=ax_ts)

    axes = [ax_ts, ax_end_period_pdf, ax_end_period_box]

    if include_peak:
        ax_peak_year_pdf = fig.add_subplot(gs[1, 0], sharex=ax_ts)
        ax_peak_year_box = fig.add_subplot(gs[2, 0], sharex=ax_ts)

        ax_peak_pdf = fig.add_subplot(gs[1:, 1], sharey=ax_ts, sharex=ax_end_period_pdf)
        ax_peak_box = fig.add_subplot(gs[1:, 2], sharey=ax_peak_pdf)

        axes += [ax_peak_year_pdf, ax_peak_year_box, ax_peak_pdf, ax_peak_box]

    if scenario_esm is None:
        calc_df = get_calc_df(data_scmrun, variable, scenario)
    else:
        calc_df = get_calc_df(
            data_scmrun, variable, [scenario, scenario_esm]
        )
    
    if obs is not None:
        obs_hacked = obs.copy()
        obs_hacked["scenario"] = obs_hacked["scenario"].apply(convert_scenario_name)
        obs_hacked["variable"] = variable
        hue_order = get_hue_order(calc_df, obs_hacked, cmip6=cmip6, cmip6_input=cmip6_input)
        sdf = scmdata.run_append([calc_df, obs_hacked]).time_mean("AC")
    else:
        hue_order = get_hue_order(calc_df, cmip6=cmip6, cmip6_input=cmip6_input)
        sdf = scmdata.run_append([calc_df]).time_mean("AC")

    sizes = get_sizes(
        hue_order, linewidth=linewidth, linewidth_cmip6=0.5
    )

    
    sdf["Source"] = sdf["climate_model"]
    
    if cmip6:
        cmip_run = cmip_timeseries.filter(variable=cmip_var, unit=unit, scenario=scenario)
        cmip_run["scenario"] = cmip_run["scenario"].apply(convert_scenario_name)

        cmip_run = get_longest_ensemble_member_for_climate_model(cmip_run, plt_years, ref_period)

        if ref_period is not None:
            cmip_run = cmip_run.relative_to_ref_period_mean(year=ref_period)
            
        cmip_run = scmdata.ScmRun(cmip_run.process_over(("member_id",), "mean"))
        cmip_run["Source"] = "CMIP6 MME"
        cmip_run["variable"] = variable

        sdf = sdf.append(cmip_run.time_mean("AC"))

        palette["CMIP6 MME"] = "tab:blue"
    
    if cmip6_input is not None:
        sdf = sdf.append(cmip6_input.time_mean("AC"))
        palette["CMIP6 ScenarioMIP inputs"] = "black"
        
    if scenario_esm is None:
        dashes = {convert_scenario_name(scenario): "", "Historical": (5, 3)}
        dashes_pdf = None
    else:
        dashes = {convert_scenario_name(scenario): "", "Historical": (5, 3), convert_scenario_name(scenario_esm): (2, 2)}
        dashes_pdf = {scenario: "", "Historical": (5, 3), scenario_esm: (2, 2)}
            
    if cmip6_input is not None:
        dashes[convert_scenario_name(cmip6_input.get_unique_meta("scenario", no_duplicates=True))] = (5, 3)
    
    palette_converted = {convert_scenario_name(k): v for k, v in palette.items()}
    sdf["Scenario"] = sdf["scenario"].apply(convert_scenario_name)
    ax_ts = make_ts_plot(
        ax_ts,
        sdf,
        ref_period,
        quantiles,
        plt_years,
        palette_converted,
        hue,
        hue_order,
        style="Scenario",
        dashes=dashes,
        alpha=0.7,
        process_over_columns=process_over_columns,
        units=units,
        size=hue,
        sizes=sizes,
    )
    ax_ts.legend(ncol=2, loc="best")
    ax_ts.set_xlabel("Year")
    
    if include_peak:
        temperature_yticks = np.arange(ylim_ts[0], ylim_ts[-1] + 0.01, bin_width)
    else:
        temperature_yticks = np.linspace(*ylim_ts, 20)

    (
        box_plot_df_end_period,
        custom_ars,
        metric_name,
    ) = calculate_custom_boxplot_ars_and_name_from_results(
        calc_df,
        variable,
        unit,
        scenario_list,
        eval_period=eval_period_end,
        ref_period=ref_period,
        eval_method="mean",
        region="World",
        include_assessed_pdf=include_assessed_pdf,
        include_oscar_custom=include_oscar_custom,
    )
    
    box_plot_stats_end_period = get_box_whisker_stats_custom_quantiles_multi_scenario(
        custom_ars,
        box_plot_df_end_period,
        metric_name,
        box_quantiles=(17, 83),
        whisker_quantiles=(5, 95),
    )

    ax_end_period_pdf = make_pdf_plot(
        ax_end_period_pdf,
        box_plot_df_end_period,
        metric_name,
        palette,
        custom_heights=custom_heights,
        hue="Source",
        legend=False,
        linewidth=2,
        bins=temperature_yticks,
        style="scenario" if scenario_esm is not None else None,
        dashes=dashes_pdf,
    )
    
    if scenario_esm is not None:
        palette_box = {}
        for s in scenario_list:
            for k in palette:
                palette_box["{} {}".format(k, s)] = palette[k]
    else:
        palette_box = copy.deepcopy(palette)
            
    ax_end_period_box = make_box_plot(
        ax_end_period_box, box_plot_stats_end_period, palette_box
    )
    if include_peak:
        ax_end_period_box.set_xticklabels([])

    if include_peak:
        (
            box_plot_df_peak,
            custom_ars,
            metric_name,
        ) = calculate_custom_boxplot_ars_and_name_from_results(
            calc_df,
            variable,
            unit,
            scenario_list,
            eval_period="peak",
            ref_period=ref_period,
            region="World",
            include_oscar_custom=include_oscar_custom,
        )
        
        box_plot_stats_peak = get_box_whisker_stats_custom_quantiles_multi_scenario(
            custom_ars,
            box_plot_df_peak,
            metric_name,
            box_quantiles=(17, 83),
            whisker_quantiles=(5, 95),
        )
            
        ax_peak_pdf = make_pdf_plot(
            ax_peak_pdf,
            box_plot_df_peak,
            metric_name,
            palette,
            custom_heights=custom_heights,
            hue="Source",
            legend=False,
            linewidth=2,
            bins=temperature_yticks,
            style="scenario" if scenario_esm is not None else None,
            ymax_min=ax_end_period_pdf.get_xlim()[1],
            dashes=dashes_pdf,
        )

        ax_peak_box = make_box_plot(
            ax_peak_box, box_plot_stats_peak, palette_box
        )

        ax_peak_box.yaxis.set_ticks(temperature_yticks, minor=True)
#         ax_end_period_box.grid(which="both", axis="y")
#         ax_peak_box.grid(which="both", axis="y")

        (
            box_plot_df_peak_year,
            custom_ars,
            metric_name,
        ) = calculate_custom_boxplot_ars_and_name_from_results(
            calc_df,
            variable,
            unit,
            scenario_list,
            eval_period="peak_time_21yr_smooth",
            #     eval_period="peak_time",
            ref_period=ref_period,
            region="World",
            include_oscar_custom=include_oscar_custom,
        )
        box_plot_stats_peak_year = get_box_whisker_stats_custom_quantiles_multi_scenario(
            custom_ars,
            box_plot_df_peak_year,
            metric_name,
            box_quantiles=(17, 83),
            whisker_quantiles=(5, 95),
        )

        ax_peak_year_pdf = make_pdf_plot(
            ax_peak_year_pdf,
            box_plot_df_peak_year,
            metric_name,
            palette,
            custom_heights=custom_heights,
            hue="Source",
            legend=False,
            linewidth=2,
            vert=False,
            bins=np.arange(2015, 2110, 5),
            style="scenario" if scenario_esm is not None else None,
            dashes=dashes_pdf,
        )
        ax_peak_year_pdf.set_xlabel("")

        ax_peak_year_box = make_box_plot(
            ax_peak_year_box, box_plot_stats_peak_year, palette_box, vert=False
        )
        ax_peak_year_box.set_xlabel(metric_name)

    ax_ts.set_ylim(*ylim_ts)
    
    add_panel_label(ax_ts, 0)
    add_panel_label(ax_end_period_pdf, 1)
    add_panel_label(ax_end_period_box, 2)
    if include_peak:
        add_panel_label(ax_peak_year_pdf, 3)
        add_panel_label(ax_peak_year_box, 4)
        add_panel_label(ax_peak_pdf, 5)
        add_panel_label(ax_peak_box, 6)
    
    return fig, axes


def get_annotation_for_panel_number(k):
    return "{})".format(string.ascii_lowercase[k])


def add_panel_label(ax, n):
    ax.annotate(
        get_annotation_for_panel_number(n),
        xy=(ax.get_xlim()[0], ax.get_ylim()[1]),
        xytext=(5, -15),
        textcoords="offset points",
        **ANNOTATION_PROPS,
        zorder=10,
    )

    
def convert_scenario_name(scenario):
    replacements = (
        ("ssp119", "SSP1-1.9"),
        ("ssp126", "SSP1-2.6"),
        ("ssp245", "SSP2-4.5"),
        ("ssp370", "SSP3-7.0"),
        ("ssp434", "SSP4-3.4"),
        ("ssp460", "SSP4-6.0"),
        ("ssp585", "SSP5-8.5"),
        ("ssp534-over", "SSP5-3.4-overshoot"),
        ("historical", "Historical"),
    )
    out = scenario
    for old, new in replacements:
        out = out.replace(old, new)
    
    return out
