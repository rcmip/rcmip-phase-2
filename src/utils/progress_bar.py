# all credit to Jared Lewis
import sys
from io import BufferedWriter

import tqdm
import tqdm.notebook


class _FileAndStderr(BufferedWriter):
    def __init__(self, handle):
        self.handle = handle
        self.handles = [handle, sys.stderr]

    def write(self, x, **kwargs):
        for h in self.handles:
            h.write(x)

    def flush(self):
        for h in self.handles:
            h.flush()

    def close(self):
        self.handle.close()


class redirect_tqdm(tqdm.tqdm):
    def __init__(self, *args, output_file="/tmp/constraining_progress.txt", **kwargs):
        """
        Redirect output to stderr and disk

        Allows for monitoring long running tasks. Make sure that a descriptive `desc` argument is provided to differentiate tasks

        Parameters
        ----------
        args:
            Extra parameters for tqdm.tqdm
        output_file : str
            File to write progress bars to.
        kwargs : Any
            Extra parameters for tqdm.tqdm
        """
        fh = open(output_file, "a")
        dummy_file = _FileAndStderr(fh)
        super(redirect_tqdm, self).__init__(*args, file=dummy_file, **kwargs)

    def close(self):
        super(redirect_tqdm, self).close()
        self.fp.close()


def get_progress_bar(notebook):
    if notebook:
        return tqdm.notebook.tqdm

    return redirect_tqdm
