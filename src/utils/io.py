import logging
import os.path


LOGGER = logging.getLogger(__name__)


def ensure_dir_exists(fp):
    dir_to_check = os.path.dirname(fp)
    if not os.path.isdir(dir_to_check):
        LOGGER.info("Creating {}".format(dir_to_check))
        os.makedirs(dir_to_check)
