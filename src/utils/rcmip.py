import functools
import os.path

import pandas as pd
from openscm_units import unit_registry


ASSESSED_RANGES_COLUMNS = [
    "RCMIP name",
    "RCMIP variable",
    "RCMIP region",
    "RCMIP scenario",
    "evaluation_period_start",
    "evaluation_period_end",
    "norm_period_start",
    "norm_period_end",
    "very_likely__lower",
    "likely__lower",
    "central",
    "likely__upper",
    "very_likely__upper",
    "unit",
    "RCMIP evaluation method",
    "Source",
]


@functools.lru_cache()
def _get_oscar_data():
    oscar_path = os.path.join(
        os.path.dirname(__file__), 
        "..", 
        "..", 
        "data", 
        "processed",
        "submission-database",
        "oscar_quantiles_processed.csv"
    )
    return pd.read_csv(oscar_path)

def make_ar(
    rcmip_name,
    rcmip_variable,
    rcmip_region,
    rcmip_scenario,
    evaluation_period_start,
    evaluation_period_end,
    norm_period_start,
    norm_period_end,
    very_likely__lower,
    likely__lower,
    central,
    likely__upper,
    very_likely__upper,
    unit,
    rcmip_evaluation_method,
    source,
):
    return pd.DataFrame(
        [
            [
                rcmip_name,
                rcmip_variable,
                rcmip_region,
                rcmip_scenario,
                evaluation_period_start,
                evaluation_period_end,
                norm_period_start,
                norm_period_end,
                very_likely__lower,
                likely__lower,
                central,
                likely__upper,
                very_likely__upper,
                unit,
                rcmip_evaluation_method,
                source,
            ]
        ],
        columns=ASSESSED_RANGES_COLUMNS,
    )


def add_oscar_reported_results(derived, metric, overwrite_oscar_metric_name=False):
    oscar_data = _get_oscar_data().copy()
    if metric in oscar_data["RCMIP name"].tolist():
        print(f"Using OSCAR reported data for {metric}")
        oscar_metric_vals = oscar_data.loc[oscar_data["RCMIP name"] == metric, :].copy()

        oscar_units = oscar_metric_vals["unit"].unique().tolist()
        derived_units = derived["unit"].unique().tolist()
        
        if "Year of peak" in metric:
            oscar_units = derived_units
            oscar_metric_vals = oscar_metric_vals
            oscar_metric_vals.loc[:, "unit"] = derived_units[0]
            
        if len(oscar_units) > 1:
            raise AssertionError(f"OSCAR: {oscar_units}")
            
        if oscar_units != derived_units:
            print(f"Convert OSCAR units {oscar_units} to {derived_units}")
            new_vals = (
                (oscar_units["value"].values * unit_registry(oscar_units[0]))
                .to(derived_units[0])
            )
            oscar_units.loc[:, "value"] = new_vals.magnitude
            oscar_units.loc[:, "unit"] = str(new_vals.units)
        
        oscar_metric_vals = oscar_metric_vals.copy()
        oscar_metric_vals["model"] = "unspecified"

        cols_to_copy = [
            "evaluation_period_end_year",
            "evaluation_period_start_year",
            "reference_period_end_year",
            "reference_period_start_year",
            "region",
            "scenario",
            "variable", 
        ]
        if overwrite_oscar_metric_name:
            cols_to_copy.append("RCMIP name")
            
        for col_to_copy in cols_to_copy:
            try:
                copy_vals = derived[col_to_copy].unique().tolist()
            except KeyError:
                continue

            assert len(copy_vals) == 1, copy_vals
            oscar_metric_vals.loc[:, col_to_copy] = copy_vals[0]

        out = pd.concat([
            oscar_metric_vals,
            derived.loc[~derived["climate_model"].str.startswith("OSCAR"), :].copy()
        ])

    else:
        print(f"Not using OSCAR reported data for {metric}")
        out = derived

    return out


def get_monkey_patched_assessed_ranges(ars):
    calculate_metric_from_results_original = ars.calculate_metric_from_results

    def calculate_metric_from_results_monkey(metric, res_calc, custom_calculators=None):
        derived = calculate_metric_from_results_original(
            metric, res_calc, custom_calculators
        )
        return add_oscar_reported_results(derived, metric)

    ars.calculate_metric_from_results = calculate_metric_from_results_monkey

    return ars
