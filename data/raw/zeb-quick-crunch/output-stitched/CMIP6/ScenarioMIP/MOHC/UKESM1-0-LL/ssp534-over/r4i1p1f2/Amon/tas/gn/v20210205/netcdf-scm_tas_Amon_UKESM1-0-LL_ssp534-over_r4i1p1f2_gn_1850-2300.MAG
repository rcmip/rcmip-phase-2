---- HEADER ----

Date: 2021-02-21 13:13:16
Contact: zebedee.nicholls@climate-energy-college.org
Source data crunched with: netCDF-SCM v2.0.0 (more info at gitlab.com/netcdf-scm/netcdf-scm)
File written with: pymagicc v2.0.0rc9 (more info at github.com/openclimatedata/pymagicc)

For more information on the AR6 regions (including mapping the abbrevations to their full names), see: https://github.com/SantanderMetGroup/ATLAS/tree/master/reference-regions, specifically https://github.com/SantanderMetGroup/ATLAS/blob/master/reference-regions/IPCC-WGI-reference-regions-v4_coordinates.csv (paper is at https://doi.org/10.5194/essd-2019-258)

---- METADATA ----

(child) Conventions: CF-1.7
(child) area_world (m**2): 509499402446956.2
(child) branch_method: standard
(child) branch_time_in_child: 68400.0
(child) branch_time_in_parent: 68400.0
(child) calendar: 360_day
(child) cmor_version: 3.4.0
(child) comment: near-surface (usually, 2 meter) air temperature
(child) crunch_contact: zebedee.nicholls@climate-energy-college.org
(child) crunch_netcdf_scm_version: 2.0.0 (more info at gitlab.com/netcdf-scm/netcdf-scm)
(child) crunch_netcdf_scm_weight_kwargs: {"regions": ["World"], "cell_weights": null}
(child) crunch_source_files: Files: ['/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_204001-204912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_205001-210012.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_210101-214912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_215001-224912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_225001-230012.nc']
(child) data_specs_version: 01.00.29
(child) experiment: overshoot of 3.4 W/m**2 branching from ssp585 in 2040
(child) experiment_id: ssp534-over
(child) external_variables: areacella
(child) forcing_index: 2
(child) frequency: mon
(child) further_info_url: https://furtherinfo.es-doc.org/CMIP6.MOHC.UKESM1-0-LL.ssp534-over.none.r4i1p1f2
(child) grid: Native N96 grid; 192 x 144 longitude/latitude
(child) grid_label: gn
(child) initialization_index: 1
(child) institution: Met Office Hadley Centre, Fitzroy Road, Exeter, Devon, EX1 3PB, UK
(child) institution_id: MOHC
(child) license: CMIP6 model data produced by the Met Office Hadley Centre is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https://ukesm.ac.uk/cmip6. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(child) mo_runid: u-bh285
(child) netcdf-scm crunched file: CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp534-over/r4i1p1f2/Amon/tas/gn/v20210205/netcdf-scm_tas_Amon_UKESM1-0-LL_ssp534-over_r4i1p1f2_gn_204001-230012.nc
(child) nominal_resolution: 250 km
(child) original_name: mo: (stash: m01s03i236, lbproc: 128)
(child) parent_activity_id: ScenarioMIP
(child) parent_experiment_id: ssp585
(child) parent_mip_era: CMIP6
(child) parent_source_id: UKESM1-0-LL
(child) parent_time_units: days since 1850-01-01-00-00-00
(child) parent_variant_label: r4i1p1f2
(child) physics_index: 1
(child) product: model-output
(child) realization_index: 4
(child) realm: atmos
(child) source: UKESM1.0-LL (2018): 
aerosol: UKCA-GLOMAP-mode
atmos: MetUM-HadGEM3-GA7.1 (N96; 192 x 144 longitude/latitude; 85 levels; top level 85 km)
atmosChem: UKCA-StratTrop
land: JULES-ES-1.0
landIce: none
ocean: NEMO-HadGEM3-GO6.0 (eORCA1 tripolar primarily 1 deg with meridional refinement down to 1/3 degree in the tropics; 360 x 330 longitude/latitude; 75 levels; top grid cell 0-1 m)
ocnBgchem: MEDUSA2
seaIce: CICE-HadGEM3-GSI8 (eORCA1 tripolar primarily 1 deg; 360 x 330 longitude/latitude)
(child) source_id: UKESM1-0-LL
(child) source_type: AOGCM AER BGC CHEM
(child) sub_experiment: none
(child) sub_experiment_id: none
(child) table_id: Amon
(child) title: UKESM1-0-LL output prepared for CMIP6
(child) variable_id: tas
(child) variable_name: tas
(child) variant_label: r4i1p1f2
(grandparent) Conventions: CF-1.7
(grandparent) area_world (m**2): 509499402446956.2
(grandparent) branch_method: standard
(grandparent) branch_time_in_child: 0.0
(grandparent) branch_time_in_parent: 39600.0
(grandparent) calendar: 360_day
(grandparent) cmor_version: 3.4.0
(grandparent) comment: near-surface (usually, 2 meter) air temperature
(grandparent) crunch_contact: zebedee.nicholls@climate-energy-college.org
(grandparent) crunch_netcdf_scm_version: 2.0.0 (more info at gitlab.com/netcdf-scm/netcdf-scm)
(grandparent) crunch_netcdf_scm_weight_kwargs: {"regions": ["World"], "cell_weights": null}
(grandparent) crunch_source_files: Files: ['/CMIP6/CMIP/MOHC/UKESM1-0-LL/historical/r4i1p1f2/Amon/tas/gn/v20190502/tas_Amon_UKESM1-0-LL_historical_r4i1p1f2_gn_185001-194912.nc', '/CMIP6/CMIP/MOHC/UKESM1-0-LL/historical/r4i1p1f2/Amon/tas/gn/v20190502/tas_Amon_UKESM1-0-LL_historical_r4i1p1f2_gn_195001-201412.nc']
(grandparent) cv_version: 6.2.20.1
(grandparent) data_specs_version: 01.00.29
(grandparent) experiment: all-forcing simulation of the recent past
(grandparent) experiment_id: historical
(grandparent) external_variables: areacella
(grandparent) forcing_index: 2
(grandparent) frequency: mon
(grandparent) further_info_url: https://furtherinfo.es-doc.org/CMIP6.MOHC.UKESM1-0-LL.historical.none.r4i1p1f2
(grandparent) grid: Native N96 grid; 192 x 144 longitude/latitude
(grandparent) grid_label: gn
(grandparent) initialization_index: 1
(grandparent) institution: Met Office Hadley Centre, Fitzroy Road, Exeter, Devon, EX1 3PB, UK
(grandparent) institution_id: MOHC
(grandparent) license: CMIP6 model data produced by the Met Office Hadley Centre is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https://ukesm.ac.uk/cmip6. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(grandparent) mo_runid: u-bb075
(grandparent) netcdf-scm crunched file: CMIP6/CMIP/MOHC/UKESM1-0-LL/historical/r4i1p1f2/Amon/tas/gn/v20190502/netcdf-scm_tas_Amon_UKESM1-0-LL_historical_r4i1p1f2_gn_185001-201412.nc
(grandparent) nominal_resolution: 250 km
(grandparent) original_name: mo: (stash: m01s03i236, lbproc: 128)
(grandparent) parent_activity_id: CMIP
(grandparent) parent_experiment_id: piControl
(grandparent) parent_mip_era: CMIP6
(grandparent) parent_source_id: UKESM1-0-LL
(grandparent) parent_time_units: days since 1850-01-01-00-00-00
(grandparent) parent_variant_label: r1i1p1f2
(grandparent) physics_index: 1
(grandparent) product: model-output
(grandparent) realization_index: 4
(grandparent) realm: atmos
(grandparent) source: UKESM1.0-LL (2018): 
aerosol: UKCA-GLOMAP-mode
atmos: MetUM-HadGEM3-GA7.1 (N96; 192 x 144 longitude/latitude; 85 levels; top level 85 km)
atmosChem: UKCA-StratTrop
land: JULES-ES-1.0
landIce: none
ocean: NEMO-HadGEM3-GO6.0 (eORCA1 tripolar primarily 1 deg with meridional refinement down to 1/3 degree in the tropics; 360 x 330 longitude/latitude; 75 levels; top grid cell 0-1 m)
ocnBgchem: MEDUSA2
seaIce: CICE-HadGEM3-GSI8 (eORCA1 tripolar primarily 1 deg; 360 x 330 longitude/latitude)
(grandparent) source_id: UKESM1-0-LL
(grandparent) source_type: AOGCM AER BGC CHEM
(grandparent) sub_experiment: none
(grandparent) sub_experiment_id: none
(grandparent) table_id: Amon
(grandparent) table_info: Creation Date:(13 December 2018) MD5:2b12b5db6db112aa8b8b0d6c1645b121
(grandparent) title: UKESM1-0-LL output prepared for CMIP6
(grandparent) variable_id: tas
(grandparent) variant_label: r4i1p1f2
(parent) Conventions: CF-1.7
(parent) area_world (m**2): 509499402446956.2
(parent) branch_method: standard
(parent) branch_time_in_child: 59400.0
(parent) branch_time_in_parent: 59400.0
(parent) calendar: 360_day
(parent) cmor_version: 3.4.0
(parent) comment: near-surface (usually, 2 meter) air temperature
(parent) crunch_contact: zebedee.nicholls@climate-energy-college.org
(parent) crunch_netcdf_scm_version: 2.0.0 (more info at gitlab.com/netcdf-scm/netcdf-scm)
(parent) crunch_netcdf_scm_weight_kwargs: {"regions": ["World"], "cell_weights": null}
(parent) crunch_source_files: Files: ['/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_201501-204912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_205001-210012.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_210101-214912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_215001-224912.nc', '/CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_225001-230012.nc']
(parent) data_specs_version: 01.00.29
(parent) experiment: update of RCP8.5 based on SSP5
(parent) experiment_id: ssp585
(parent) external_variables: areacella
(parent) forcing_index: 2
(parent) frequency: mon
(parent) further_info_url: https://furtherinfo.es-doc.org/CMIP6.MOHC.UKESM1-0-LL.ssp585.none.r4i1p1f2
(parent) grid: Native N96 grid; 192 x 144 longitude/latitude
(parent) grid_label: gn
(parent) initialization_index: 1
(parent) institution: Met Office Hadley Centre, Fitzroy Road, Exeter, Devon, EX1 3PB, UK
(parent) institution_id: MOHC
(parent) license: CMIP6 model data produced by the Met Office Hadley Centre is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License (https://creativecommons.org/licenses). Consult https://pcmdi.llnl.gov/CMIP6/TermsOfUse for terms of use governing CMIP6 output, including citation requirements and proper acknowledgment. Further information about this data, including some limitations, can be found via the further_info_url (recorded as a global attribute in this file) and at https://ukesm.ac.uk/cmip6. The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law.
(parent) mo_runid: u-be392
(parent) netcdf-scm crunched file: CMIP6/ScenarioMIP/MOHC/UKESM1-0-LL/ssp585/r4i1p1f2/Amon/tas/gn/v20210205/netcdf-scm_tas_Amon_UKESM1-0-LL_ssp585_r4i1p1f2_gn_201501-230012.nc
(parent) nominal_resolution: 250 km
(parent) original_name: mo: (stash: m01s03i236, lbproc: 128)
(parent) parent_activity_id: CMIP
(parent) parent_experiment_id: historical
(parent) parent_mip_era: CMIP6
(parent) parent_source_id: UKESM1-0-LL
(parent) parent_time_units: days since 1850-01-01-00-00-00
(parent) parent_variant_label: r4i1p1f2
(parent) physics_index: 1
(parent) product: model-output
(parent) realization_index: 4
(parent) realm: atmos
(parent) source: UKESM1.0-LL (2018): 
aerosol: UKCA-GLOMAP-mode
atmos: MetUM-HadGEM3-GA7.1 (N96; 192 x 144 longitude/latitude; 85 levels; top level 85 km)
atmosChem: UKCA-StratTrop
land: JULES-ES-1.0
landIce: none
ocean: NEMO-HadGEM3-GO6.0 (eORCA1 tripolar primarily 1 deg with meridional refinement down to 1/3 degree in the tropics; 360 x 330 longitude/latitude; 75 levels; top grid cell 0-1 m)
ocnBgchem: MEDUSA2
seaIce: CICE-HadGEM3-GSI8 (eORCA1 tripolar primarily 1 deg; 360 x 330 longitude/latitude)
(parent) source_id: UKESM1-0-LL
(parent) source_type: AOGCM AER BGC CHEM
(parent) sub_experiment: none
(parent) sub_experiment_id: none
(parent) table_id: Amon
(parent) title: UKESM1-0-LL output prepared for CMIP6
(parent) variable_id: tas
(parent) variant_label: r4i1p1f2

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 1
    THISFILE_DATAROWS = 451
    THISFILE_FIRSTYEAR = 1850
    THISFILE_LASTYEAR = 2300
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'FOURBOX'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 200
/

   VARIABLE                 tas
       TODO                 SET
      UNITS                   K
      YEARS               WORLD
       1850         2.86961e+02
       1851         2.86787e+02
       1852         2.86892e+02
       1853         2.87094e+02
       1854         2.86866e+02
       1855         2.86759e+02
       1856         2.86711e+02
       1857         2.86762e+02
       1858         2.86823e+02
       1859         2.86874e+02
       1860         2.86702e+02
       1861         2.86606e+02
       1862         2.86533e+02
       1863         2.86621e+02
       1864         2.86716e+02
       1865         2.86705e+02
       1866         2.86570e+02
       1867         2.86536e+02
       1868         2.86605e+02
       1869         2.86662e+02
       1870         2.86825e+02
       1871         2.86883e+02
       1872         2.86909e+02
       1873         2.86851e+02
       1874         2.86747e+02
       1875         2.86636e+02
       1876         2.86589e+02
       1877         2.86698e+02
       1878         2.86656e+02
       1879         2.86657e+02
       1880         2.86753e+02
       1881         2.86714e+02
       1882         2.86691e+02
       1883         2.86504e+02
       1884         2.86405e+02
       1885         2.86653e+02
       1886         2.86622e+02
       1887         2.86640e+02
       1888         2.86626e+02
       1889         2.86675e+02
       1890         2.86701e+02
       1891         2.86429e+02
       1892         2.86646e+02
       1893         2.86620e+02
       1894         2.86500e+02
       1895         2.86484e+02
       1896         2.86591e+02
       1897         2.86539e+02
       1898         2.86745e+02
       1899         2.86716e+02
       1900         2.86602e+02
       1901         2.86708e+02
       1902         2.86871e+02
       1903         2.86625e+02
       1904         2.86549e+02
       1905         2.86667e+02
       1906         2.86663e+02
       1907         2.86598e+02
       1908         2.86577e+02
       1909         2.86619e+02
       1910         2.86631e+02
       1911         2.86565e+02
       1912         2.86453e+02
       1913         2.86381e+02
       1914         2.86289e+02
       1915         2.86525e+02
       1916         2.86725e+02
       1917         2.86548e+02
       1918         2.86604e+02
       1919         2.86712e+02
       1920         2.86619e+02
       1921         2.86532e+02
       1922         2.86620e+02
       1923         2.86601e+02
       1924         2.86513e+02
       1925         2.86678e+02
       1926         2.86784e+02
       1927         2.86512e+02
       1928         2.86518e+02
       1929         2.86654e+02
       1930         2.86665e+02
       1931         2.86462e+02
       1932         2.86538e+02
       1933         2.86601e+02
       1934         2.86672e+02
       1935         2.86746e+02
       1936         2.86730e+02
       1937         2.86804e+02
       1938         2.86777e+02
       1939         2.86745e+02
       1940         2.86762e+02
       1941         2.86721e+02
       1942         2.86725e+02
       1943         2.86795e+02
       1944         2.86924e+02
       1945         2.86771e+02
       1946         2.86653e+02
       1947         2.86704e+02
       1948         2.86796e+02
       1949         2.86811e+02
       1950         2.86826e+02
       1951         2.86966e+02
       1952         2.86879e+02
       1953         2.86637e+02
       1954         2.86769e+02
       1955         2.86695e+02
       1956         2.86712e+02
       1957         2.86792e+02
       1958         2.86937e+02
       1959         2.86765e+02
       1960         2.86660e+02
       1961         2.86708e+02
       1962         2.86842e+02
       1963         2.86799e+02
       1964         2.86361e+02
       1965         2.86455e+02
       1966         2.86357e+02
       1967         2.86362e+02
       1968         2.86536e+02
       1969         2.86369e+02
       1970         2.86323e+02
       1971         2.86410e+02
       1972         2.86428e+02
       1973         2.86454e+02
       1974         2.86347e+02
       1975         2.86465e+02
       1976         2.86488e+02
       1977         2.86420e+02
       1978         2.86489e+02
       1979         2.86472e+02
       1980         2.86516e+02
       1981         2.86584e+02
       1982         2.86551e+02
       1983         2.86482e+02
       1984         2.86530e+02
       1985         2.86555e+02
       1986         2.86575e+02
       1987         2.86534e+02
       1988         2.86685e+02
       1989         2.86836e+02
       1990         2.86608e+02
       1991         2.86616e+02
       1992         2.86600e+02
       1993         2.86628e+02
       1994         2.86589e+02
       1995         2.86724e+02
       1996         2.86938e+02
       1997         2.86903e+02
       1998         2.86953e+02
       1999         2.86965e+02
       2000         2.87148e+02
       2001         2.87274e+02
       2002         2.87271e+02
       2003         2.87442e+02
       2004         2.87465e+02
       2005         2.87462e+02
       2006         2.87540e+02
       2007         2.87562e+02
       2008         2.87351e+02
       2009         2.87235e+02
       2010         2.87234e+02
       2011         2.87381e+02
       2012         2.87615e+02
       2013         2.87410e+02
       2014         2.87490e+02
       2015         2.87683e+02
       2016         2.87628e+02
       2017         2.87700e+02
       2018         2.87820e+02
       2019         2.87906e+02
       2020         2.87828e+02
       2021         2.87840e+02
       2022         2.87972e+02
       2023         2.88005e+02
       2024         2.87986e+02
       2025         2.88024e+02
       2026         2.88215e+02
       2027         2.88200e+02
       2028         2.88179e+02
       2029         2.88311e+02
       2030         2.88524e+02
       2031         2.88501e+02
       2032         2.88611e+02
       2033         2.88692e+02
       2034         2.88956e+02
       2035         2.88925e+02
       2036         2.88922e+02
       2037         2.88970e+02
       2038         2.89201e+02
       2039         2.89236e+02
       2040         2.89162e+02
       2041         2.89167e+02
       2042         2.89148e+02
       2043         2.89145e+02
       2044         2.89349e+02
       2045         2.89475e+02
       2046         2.89531e+02
       2047         2.89441e+02
       2048         2.89531e+02
       2049         2.89554e+02
       2050         2.89515e+02
       2051         2.89828e+02
       2052         2.89947e+02
       2053         2.90019e+02
       2054         2.90020e+02
       2055         2.89960e+02
       2056         2.90085e+02
       2057         2.90145e+02
       2058         2.90065e+02
       2059         2.90072e+02
       2060         2.90169e+02
       2061         2.90284e+02
       2062         2.90165e+02
       2063         2.90154e+02
       2064         2.90246e+02
       2065         2.90417e+02
       2066         2.90274e+02
       2067         2.90293e+02
       2068         2.90208e+02
       2069         2.90020e+02
       2070         2.90239e+02
       2071         2.90211e+02
       2072         2.89976e+02
       2073         2.89963e+02
       2074         2.90256e+02
       2075         2.90188e+02
       2076         2.90226e+02
       2077         2.90117e+02
       2078         2.90261e+02
       2079         2.90328e+02
       2080         2.90059e+02
       2081         2.90088e+02
       2082         2.90303e+02
       2083         2.90044e+02
       2084         2.90111e+02
       2085         2.90213e+02
       2086         2.89996e+02
       2087         2.89965e+02
       2088         2.90035e+02
       2089         2.90008e+02
       2090         2.89890e+02
       2091         2.89896e+02
       2092         2.90011e+02
       2093         2.89991e+02
       2094         2.90043e+02
       2095         2.90076e+02
       2096         2.89947e+02
       2097         2.89886e+02
       2098         2.89923e+02
       2099         2.90016e+02
       2100         2.89786e+02
       2101         2.89878e+02
       2102         2.89901e+02
       2103         2.89733e+02
       2104         2.89730e+02
       2105         2.89773e+02
       2106         2.89840e+02
       2107         2.89731e+02
       2108         2.89766e+02
       2109         2.89927e+02
       2110         2.89575e+02
       2111         2.89673e+02
       2112         2.89854e+02
       2113         2.89792e+02
       2114         2.89724e+02
       2115         2.89686e+02
       2116         2.89589e+02
       2117         2.89609e+02
       2118         2.89525e+02
       2119         2.89671e+02
       2120         2.89654e+02
       2121         2.89548e+02
       2122         2.89526e+02
       2123         2.89470e+02
       2124         2.89334e+02
       2125         2.89378e+02
       2126         2.89651e+02
       2127         2.89613e+02
       2128         2.89574e+02
       2129         2.89502e+02
       2130         2.89535e+02
       2131         2.89414e+02
       2132         2.89373e+02
       2133         2.89506e+02
       2134         2.89507e+02
       2135         2.89344e+02
       2136         2.89374e+02
       2137         2.89426e+02
       2138         2.89360e+02
       2139         2.89235e+02
       2140         2.89265e+02
       2141         2.89350e+02
       2142         2.89251e+02
       2143         2.89218e+02
       2144         2.89186e+02
       2145         2.89170e+02
       2146         2.89126e+02
       2147         2.89202e+02
       2148         2.89290e+02
       2149         2.89245e+02
       2150         2.89284e+02
       2151         2.89132e+02
       2152         2.89104e+02
       2153         2.89234e+02
       2154         2.89145e+02
       2155         2.89134e+02
       2156         2.89052e+02
       2157         2.89140e+02
       2158         2.89049e+02
       2159         2.89266e+02
       2160         2.89270e+02
       2161         2.89291e+02
       2162         2.89085e+02
       2163         2.89030e+02
       2164         2.89036e+02
       2165         2.89222e+02
       2166         2.89393e+02
       2167         2.89211e+02
       2168         2.89104e+02
       2169         2.89104e+02
       2170         2.89158e+02
       2171         2.89023e+02
       2172         2.89171e+02
       2173         2.89121e+02
       2174         2.89118e+02
       2175         2.89067e+02
       2176         2.89067e+02
       2177         2.89296e+02
       2178         2.89128e+02
       2179         2.89185e+02
       2180         2.89170e+02
       2181         2.89294e+02
       2182         2.89387e+02
       2183         2.89138e+02
       2184         2.88937e+02
       2185         2.88933e+02
       2186         2.89040e+02
       2187         2.88969e+02
       2188         2.89092e+02
       2189         2.89153e+02
       2190         2.89024e+02
       2191         2.89150e+02
       2192         2.89262e+02
       2193         2.89135e+02
       2194         2.89061e+02
       2195         2.89136e+02
       2196         2.89282e+02
       2197         2.89219e+02
       2198         2.89213e+02
       2199         2.89099e+02
       2200         2.89167e+02
       2201         2.89189e+02
       2202         2.89052e+02
       2203         2.89044e+02
       2204         2.89070e+02
       2205         2.89173e+02
       2206         2.89129e+02
       2207         2.89071e+02
       2208         2.89177e+02
       2209         2.89320e+02
       2210         2.89159e+02
       2211         2.89041e+02
       2212         2.89254e+02
       2213         2.89423e+02
       2214         2.89305e+02
       2215         2.89286e+02
       2216         2.89322e+02
       2217         2.89326e+02
       2218         2.89237e+02
       2219         2.89095e+02
       2220         2.89060e+02
       2221         2.89010e+02
       2222         2.89031e+02
       2223         2.89214e+02
       2224         2.89221e+02
       2225         2.89122e+02
       2226         2.89149e+02
       2227         2.89096e+02
       2228         2.89033e+02
       2229         2.88958e+02
       2230         2.89085e+02
       2231         2.89281e+02
       2232         2.89271e+02
       2233         2.89205e+02
       2234         2.89176e+02
       2235         2.89302e+02
       2236         2.89070e+02
       2237         2.89008e+02
       2238         2.88999e+02
       2239         2.89154e+02
       2240         2.89167e+02
       2241         2.88989e+02
       2242         2.89149e+02
       2243         2.89040e+02
       2244         2.89207e+02
       2245         2.89118e+02
       2246         2.89116e+02
       2247         2.89117e+02
       2248         2.89132e+02
       2249         2.89046e+02
       2250         2.89044e+02
       2251         2.88887e+02
       2252         2.89029e+02
       2253         2.89179e+02
       2254         2.88988e+02
       2255         2.88923e+02
       2256         2.89098e+02
       2257         2.89153e+02
       2258         2.89214e+02
       2259         2.89071e+02
       2260         2.89186e+02
       2261         2.89219e+02
       2262         2.89127e+02
       2263         2.89043e+02
       2264         2.89149e+02
       2265         2.89273e+02
       2266         2.89238e+02
       2267         2.89132e+02
       2268         2.89171e+02
       2269         2.89225e+02
       2270         2.89321e+02
       2271         2.89365e+02
       2272         2.89289e+02
       2273         2.89207e+02
       2274         2.89305e+02
       2275         2.89267e+02
       2276         2.89058e+02
       2277         2.89130e+02
       2278         2.89150e+02
       2279         2.89092e+02
       2280         2.89141e+02
       2281         2.89142e+02
       2282         2.89190e+02
       2283         2.89273e+02
       2284         2.89179e+02
       2285         2.89134e+02
       2286         2.89065e+02
       2287         2.89211e+02
       2288         2.89314e+02
       2289         2.89042e+02
       2290         2.89112e+02
       2291         2.89219e+02
       2292         2.89099e+02
       2293         2.89244e+02
       2294         2.89129e+02
       2295         2.89119e+02
       2296         2.89143e+02
       2297         2.89043e+02
       2298         2.89121e+02
       2299         2.89107e+02
       2300         2.89089e+02
