# Zeb quick crunched data

A couple of extra data files which didn't make it 
through the cmip6.science.unimelb.edu.au crunching
for reasons described at https://gitlab.com/netcdf-scm/netcdf-scm/-/issues/56.

