#!/bin/bash
#
# Upload the repository to Zenodo
#

ROOT_DIR=$(dirname "$0")/..
ROOT_DIR=$(realpath $ROOT_DIR)
echo $ROOT_DIR
ARCHIVE_DIR=$ROOT_DIR/../rcmip-phase-2-archive
TAR_FILE=$ROOT_DIR/../rcmip-phase-2.tar.gz

echo "Will prepare archive in $ARCHIVE_DIR"
mkdir -p "$ARCHIVE_DIR"

archive_tar=true
archive_tar=false
single_tar=true
#single_tar=false

rsync -axv --delete \
    --exclude "*.env" \
    --exclude "*.ipynb_checkpoints*" \
    --exclude "data/raw/hadcrut4" \
    --exclude ".git" \
    --exclude "*__pycache__*" \
    --exclude "*egg-info*" \
    "${ROOT_DIR}/../rcmip-phase-2" \
    "${ARCHIVE_DIR}"

if [ -f .env ]
then
  # thank you https://stackoverflow.com/a/20909045
  echo "Reading .env file"
  export $(grep -v '^#' .env | xargs)
fi

PROTOCOL_METADATA_FILE="rcmip-phase-2-metadata.json"
DEPOSITION_ID_OLD="4269711"

DEPOSITION_ID=$(openscm-zenodo create-new-version "${DEPOSITION_ID_OLD}" "${PROTOCOL_METADATA_FILE}" --zenodo-url zenodo.org)
echo "DEPOSITION_ID = ${DEPOSITION_ID}"

BUCKET_ID=$(openscm-zenodo get-bucket ${DEPOSITION_ID} --zenodo-url zenodo.org)
echo "BUCKET_ID = ${BUCKET_ID}"


if [ "${archive_tar}" = true ]; then
    
    # submissions data 
    while IFS='' read -r -d '' subdir; do
    
      dirname=$(dirname "$subdir")
      second_dirname=$(basename $dirname)
      bname=$(basename "$subdir")

      for level in $subdir/*; do

          bname_level=$(basename "$level")
          tar -C $ARCHIVE_DIR -czvf "data-${second_dirname}-$bname-$bname_level.tar.gz" "$level"

      done

    done < <(find $ARCHIVE_DIR/rcmip-phase-2/data -maxdepth 2 -mindepth 2 -type d -name "submission*" -print0)
    # data except submissions
    find $ARCHIVE_DIR/rcmip-phase-2/data -maxdepth 2 -mindepth 2 -type d ! -name "submission*" -exec sh -c 'x="{}"; name=$(basename "$x" ); dirname=$(dirname "$x"); second_dirname=$(basename $dirname); tar -C $ARCHIVE_DIR -czvf "rcmip-phase-2-data-${second_dirname}-$name.tar.gz" $x' {} \;
    # everything except data in directories
    find $ARCHIVE_DIR -maxdepth 2 -mindepth 2 -type d ! -name 'data' -exec sh -c 'x="{}"; name=$(basename "$x" ); tar -C $ARCHIVE_DIR -czvf "rcmip-phase-2-${name}.tar.gz" $x' {} \;
    # everything else
    cd $ARCHIVE_DIR; find -mindepth 2 -maxdepth 2 -type f | xargs tar -C $ARCHIVE_DIR -czvf rcmip-phase-2-other.tar.gz; cd $ROOT_DIR

fi

mv *.tar.gz $ARCHIVE_DIR/

if [ "${single_tar}" = true ]; then


    for f in ${ARCHIVE_DIR}/*.tar.gz; do

        echo "Uploading tar file $f"
        echo $f

        openscm-zenodo upload "${f}" "${BUCKET_ID}" --zenodo-url zenodo.org
        # in case zenodo is failing
        echo "${f}"
        echo https://zenodo.org/api/files/${BUCKET_ID}/$(basename $f)?access_token=$ZENODO_TOKEN
#        curl --progress-bar -o /dev/null --upload-file "${f}" https://zenodo.org/api/files/${BUCKET_ID}/$(basename $f)?access_token=$ZENODO_TOKEN

    done

else

    cd ${ARCHIVE_DIR} && find -type f -exec openscm-zenodo upload {} "${BUCKET_ID}" --zenodo-url zenodo.org \;

fi
