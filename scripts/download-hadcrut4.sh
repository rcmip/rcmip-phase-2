#!/bin/bash

OUT_DIR=data/raw/hadcrut4

mkdir -p ${OUT_DIR}

file="https://www.metoffice.gov.uk/hadobs/hadcrut4/data/current/time_series/HadCRUT.4.6.0.0.annual_ns_avg.txt"
out="${OUT_DIR}/HadCRUT.4.6.0.0.annual_ns_avg.txt"
curl $file --output $out
