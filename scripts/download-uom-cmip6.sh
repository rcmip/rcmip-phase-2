#!/bin/bash

VERSION="20200920"
OUTPUT_DIR="./data/raw/cmip-unimelb/${VERSION}"

# copy files which didn't make it through on cmip6.science.unimelb.edu.au
# because of https://gitlab.com/netcdf-scm/netcdf-scm/-/issues/56
QUICK_CRUNCH_DIR="data/raw/zeb-quick-crunch/output-stitched/CMIP6"
find "${QUICK_CRUNCH_DIR}" -name '*.MAG' -printf '%P\n' | cpio --directory "${QUICK_CRUNCH_DIR}" -pd "${OUTPUT_DIR}/average-year-mid-year/CMIP6/CMIP"

declare -a models=( \
    "*" \
)

declare -a variables=( \
#     "tas" \
#     "ts" \
#     "hfds" \
    "co2" \
    "co2s" \
    "co2mass" \
)


declare -a experiments=( \
    "abrupt-0p5xCO2" \
    "abrupt-2xCO2" \
    "abrupt-4xCO2" \
    "1pctCO2" \
    "historical" \
    "ssp119" \
    "ssp126" \
    "ssp245" \
    "ssp370" \
    "ssp434" \
    "ssp460" \
    "ssp534-over" \
    "ssp585" \
    "esm-ssp119" \
    "esm-ssp126" \
    "esm-ssp245" \
    "esm-ssp370" \
    "esm-ssp434" \
    "esm-ssp460" \
    "esm-ssp534-over" \
    "esm-ssp585" \
)


for model in "${models[@]}"
do
    echo "Model: $model"

    include=( )

    for variable in "${variables[@]}"
    do

        for experiment in "${experiments[@]}"
        do

            include_str="*_${variable}_*_${model}_${experiment}_*"
            include+=( --include "${include_str}" )

        done

    done

    echo "${include[@]}"
    echo "Retrieving ascii"
    aws s3 sync \
        "s3://cmip6-calibration-data/CMIP6/${VERSION}/ascii/mag" \
        "${OUTPUT_DIR}" \
        --exclude "*" \
        "${include[@]}" \
        --exclude "*monthly*"
        
    echo "Retrieving zombimon"
    aws s3 sync \
        "s3://cmip6-calibration-data/CMIP6/${VERSION}/zombimon/mag" \
        "${OUTPUT_DIR}" \
        --exclude "*" \
        "${include[@]}" \
        --exclude "*monthly*"

done