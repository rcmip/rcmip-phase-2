.DEFAULT_GOAL := help

NOTEBOOKS_DIR=./notebooks

CONDA_ENV_YML=environment.yml


ifndef CONDA_PREFIX
$(error Conda environment not active. Activate your conda environment before using this Makefile.)
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Activate a different conda environment and rerun make. A new environment can be created with e.g. `conda create --name rcmip-phase-2 python=3.7`.))
endif
VENV_DIR=$(CONDA_PREFIX)
endif


PYTHON=$(VENV_DIR)/bin/python


define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


.PHONY: help
help:  ## show help messages for all targets
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


.PHONY: setup
setup: conda-environment  ## setup environment for running


.PHONY: conda-environment
conda-environment:  $(VENV_DIR)  ## make conda environment

.PHONY: archive
archive: rcmip-phase-2-archive.tar.gz
rcmip-phase-2-archive.tar.gz:  ## make a tar file of the repository (excluding huge sources which can be downloaded)
	tar -czvf rcmip-phase-2-archive.tar.gz MANIFEST.in README.md notebooks setup.cfg src Makefile environment.yml paper scripts setup.py versioneer.py --exclude data/raw/submissions --exclude data/raw/hadcrut4 --exclude data/raw/cmip-unimelb  data

$(VENV_DIR): $(CONDA_ENV_YML) setup.py
	$(CONDA_EXE) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_YML)
	$(VENV_DIR)/bin/pip install -e .
	touch $(VENV_DIR)
