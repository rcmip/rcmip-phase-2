This exercise is a first step towards more comprehensive, routine evaluation of RCMs’ probabilistic parameter ensembles and their corresponding projections.
However, there is still much room for future work to improve on this study and the first phase of RCMIP.
As a first suggestion, repeating this exercise with the assessed ranges from Working Group 1 of the Intergovernmental Panel on Climate Change’s Sixth Assessment Report (due in mid 2021) would provide an evaluation of the extent to which RCMs can capture the latest international assessment of the scientific literature.

This future work could go beyond evaluation and also diagnose the root causes of differences between the models.
One obvious area for examination would be the aerosol ERF, particularly the inclusion of a climate feedback in aerosol ERF parameterisations.
Such an exercise could also provide greater insights into differences between the constrained RCMs’ probabilistic distributions, the raw CMIP6 multi-model ensemble and constrained CMIP6 output (building on the discussion in Section \ref{sub:further_discussion}).

A clear limitation of this study is the relative lack of examination of carbon cycle behaviour and carbon cycle related metrics.
Given the importance of the carbon cycle for emissions-driven projections, this is another clear area for future work.
In the limited examination we have performed, we chose to focus on emissions-driven simulations.
This choice provides the cleanest comparison between RCMs and CMIP6 models, given that many RCMs do not separate the land and ocean carbon pools, although it limits us to a relatively small set of CMIP6-comparison data (given that only few emissions-driven simulations \cite{jones_2016_c4mip} have been run by CMIP6 models).
An increase in the number of emissions-driven CMIP6 ESM model output, particularly for mitigation scenarios, would greatly aid such evaluations.
Using the concentration-driven simulations in future work will also provide a greater set of comparison data and will facilitate evaluation of RCMs’ land and ocean carbon cycles under more varied scenarios.

Finally, given how RCMs are typically used by WG3, it appears that a truly thorough evaluation would need to consider a larger set of individual steps in the emissions-climate change cause-effect chain.
Such an evaluation would provide insights into the drivers of differences between future projections based on the concentration-driven experiments typical of CMIP and results based on the all greenhouse gas emissions-driven experiments required by WG3.
While it is not completely clear to us which components would need to be considered (and which could be ignored), a first suggestion of important components is: the carbon cycle, other earth system feedbacks e.g. representation of permafrost, representation of aerosols, non-\CO2 greenhouse gas cycles, translation between changes in greenhouse gas concentrations and effective radiative forcing, ozone representation, land-use change albedo representation, temperature response to effective radiative forcing and all the feedbacks and interactions.
To see the full picture, a broad range of literature would need to be considered as a validation source and a wide range of experiments, spanning historical, scenario-based and idealised experiments, would need to be performed.
In performing a more thorough evaluation, an updated evaluation technique may be required.
Specifically, using percentage differences from the assessed range will lead to problems when the assessed range is close to or spans zero.
Hence, more sophisticated ways of evaluating the agreement between model results and assessed ranges may be required.
For reasons of scope, we haven’t achieved such a thorough evaluation here, but we hope that this work provides a basis upon which future work can aim for the lofty goal of more complete evaluation of all of the relevant parts of the climate system.
