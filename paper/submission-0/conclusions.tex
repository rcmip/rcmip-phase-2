We have found that the best performing RCMs can match our proxy assessment across a range of climate metrics.
However, no RCM matched the proxy assessment across all metrics.
At the same time, all RCMs had at least one strength where they matched the proxy assessment well.

Our evaluation of probabilistic projections from RCMs provides a comparison where, for the first time, each reduced complexity modelling team knew the target distributions before developing and submitting their results.
This exercise provides a unique insight into RCMs probabilistic parameter ensembles, specifically how they compare with the target distributions and their implications for climate projections across a range of climate variables and scenarios.

Notably, we found that agreement with the proxy assessment, i.e. past performance, did not determine future performance (i.e. projections) from the RCMs.
Given the various model structure that the reduced complexity models employ, ranging from linearised impulse response functions to 50-layer ocean models, it is not surprising that models may diverge in scenarios that go significantly beyond the domain of the validation data.
Adding constraints on future performance i.e. extending the domain of validation data (for example based on an independent assessment of warming in a limited subset of scenarios) would likely reduce the divergence.
Deciding which projections are most likely to be correct will never be an exact science.
While exercises such as the one performed here can provide helpful information about where the biases may lie, they cannot provide definitive answers about what the future holds.
Those who use RCMs for climate projections should carefully consider how they’re going to use the RCMs and how they’re going to validate them before making conclusions about the implications of their projections.

In addition, we found that many of the RCMs did not reproduce the high, long-term warming seen in CMIP6 models under high-emissions scenarios.
Given that the exception was MAGICC7, it appears that its state-dependent climate sensitivity is a key feature for replicating CMIP6-style high-warming responses.
Beyond the question of temperature projections, we found that the prescribed \CO2 concentrations used in the CMIP6 SSP-based experiments are at the high-end of projections made with historically constrained carbon cycles.
Finally, we observed that a change in reference period significantly altered how well some models agreed with observations, reinforcing the need to consider more than one reference period when evaluating models.

With sufficient validations, RCMs provide a unique synthesis tool to integrate the latest scientific understanding, including its uncertainties, along the complex cause-effect chain from emissions to global-mean temperatures.
Integrating this understanding in an internally consistent RCM framework, with all the implicit cross-correlations, is our best method to inform decision-making and other scientific domains, for example the likelihood of exceeding a given global-mean temperature threshold under a specific emissions scenario.
Further developing these tools opens vast opportunities to go beyond global-mean variables and temperature changes, and to robustly represent the complex science beneath.
