Coupled Earth System Models (ESMs) have evolved for decades as primary climate research tools \cite{edwards_2000_brief}.
They represent the state of the art of complex Earth system modelling.
Nonetheless, they are not the tool of choice to assess the full breadth of scenario and Earth system response uncertainty that has been identified in the scientific literature.
It is infeasible to assess the climate implications of hundreds to thousands of emissions scenarios with the world’s most comprehensive ESMs, such as those participating in the Sixth Phase of the Couple Model Intercomparison Project (CMIP6) \cite{eyring_2016_fjksdl}, because of ESMs’ computational cost, the complexity in setting up input data and the sheer volume of output data generated.
Yet, such assessments are vital for understanding the consequences of various policy choices and their residual climate hazards.

Similarly, while some ESMs perform large, perturbed physics experiments \cite<e.g.,>[]{stainforth_2005_uncertainty} that aim to explore the full range of potential Earth system long-term annual-average responses, the ability to capture full uncertainty ranges is limited.
The ability to capture full uncertainty ranges is limited because these ESMs are relatively rigid in their structure - lacking a representation of uncertainties in vital components like the carbon cycle or effective radiative forcings.

An answer to both of these challenges, i.e. (a) limited computational resources and (b) structural scope and flexibility to represent long-term uncertainties in key metrics like global-mean surface air temperatures, are Reduced Complexity Models (RCMs), often also referred to as simple climate models (SCMs).
RCMs can play the vital role of extending the knowledge and uncertainties from multiple domains, particularly a multitude of ESM experiments, to probabilistic long-term climate projections of key variables over a wide range of scenarios (see Section 2 in \cite{Meinshausen_2011_p38saa} for other uses of RCMs).

Typically, RCMs achieve this computational efficiency and structural flexibility by limiting their spatial and temporal domains to global-mean, annual-mean quantities i.e the domains of relevance to long-term, global climate change.
Rather than aiming to represent the physics of the climate system at the process level and high-resolution, RCMs use parameterisations of the system which capture its large-scale behaviour at a greatly reduced computational cost.
This allows them to perform 350-year long simulations in a fraction of a second on a single CPU, multiple orders of magnitude faster than our most comprehensive ESMs which would take weeks to months on the world’s most advanced supercomputers.

A key example of large-scale emissions scenario assessment, and the one we focus on in this paper, is the climate assessment of socioeconomic scenarios by the Intergovernmental Panel on Climate Change (IPCC) Working Group 3 (WG3).
Hundreds of emission scenarios were assessed in the IPCC’s Fifth Assessment Report (AR5, see \citeA{IPCC_AR5_WG3_Ch6}) as well as its more recent Special Report on Global Warming of 1.5\textdegree C (SR1.5, see \citeA{IPCC_2018_SR15_Ch_2,SR15_database}).
(Scenario data is available at \url{https://secure.iiasa.ac.at/web-apps/ene/AR5DB} and \url{https://data.ene.iiasa.ac.at/iamc-1.5c-explorer/} for AR5 and SR1.5 respectively, both databases are hosted by the IIASA Energy Program).
For the IPCC’s forthcoming Sixth Assessment (AR6), it is anticipated that the number of scenarios will be in the several hundreds to a thousand (an initial snapshot of scenarios based on the SSPs is available at \url{https://tntcat.iiasa.ac.at/SspDb}).

One further reason that the world’s most comprehensive ESMs would have difficulty running WG3-type scenarios is because greenhouse gas cycles, atmospheric chemistry and dynamic vegetation modules would be required to run the WG3 emission scenarios.
While some ESMs have the required components, they are rarely used for long-term experiments for reasons of computational cost.
The most comprehensive RCMs include parameterised representations of the required components, enabling the exploration of interacting uncertainties from multiple parts of the climate system in an internally consistent setup.

In general, RCMs do not include the detail of ESMs across the emissions-climate change cause-effect chain, but they do tend to include uncertainty representations for more steps in the chain (i.e. RCMs tradeoff depth for breadth compared to ESMs).
For example, many RCMs include the relationship between methane emissions and concentrations (including temperature and other feedbacks) whereas few ESMs do in their long-term experiments.
On the other hand, few RCMs directly use land-cover information within their carbon cycles, and none consider it in the detailed way which ESMs do.
In addition, there are clearly applications where RCMs are not a feasible tool.
For example, near-term attribution studies, such as the World Weather Attribution project \cite{uhe_2016_utilising}.
For this latter application, large-ensemble ESM runs are vital - as only they can reflect natural variability and weather patterns.
Overall, there is no question that ESMs are by far the most important research tool to project future climate change.
RCMs complement the ESM efforts.
Within this paper, we focus on a very specific niche of this complementing role, i.e. synthesising multiple lines of evidence across the emissions-climate change cause-effect chain.

Within the IPCC, RCMs’ synthesising niche facilitates the transfer of knowledge from Working Group I (WG1), which assesses the physical science of the climate system, to WG3, which assesses the socioeconomics of climate change mitigation.
The knowledge transfer ensures that WG3’s scenario classification is consistent with the physical science assessment of WG1 - a key precondition to have confidence that WG3’s conclusions about the socioeconomic transformation required to mitigate anthropogenic climate change to specific levels are based on our latest scientific understanding.
Here, we describe RCMs as `integrators of knowledge' because they integrate (a relevant sub-section of) the assessment from WG1, providing WG3 with a tool that can be used for assessing the climate implications, particularly global-mean temperature changes, of a wide range of emissions scenarios.

Typically, RCMs perform this knowledge integration using probabilistic distributions, which are distinct from the emulator mode in which RCMs can also be run (see \citeA{nicholls_2020_rcmip1} for a discussion of emulation with RCMs).
These probabilistic distributions are derived by running an RCM with a parameter ensemble which captures the assessed ranges of specific Earth system quantities, e.g. historical global mean temperature increase, effective radiative forcing due to different anthropogenic emissions, ocean heat uptake, or cumulative land and ocean carbon uptake.
The resulting distributions are designed to facilitate WG3’s scenario classification e.g. to capture the likelihood that different warming levels are reached under a specific emissions scenario (e.g. 50\% and 66\%) based on the combined available evidence (in this case the WG1 assessment).
As a result of their probabilistic nature, the ensembles resulting from RCMs are conceptually different from an ensemble of multiple model outputs (such as those from CMIP6) taken without constraining or any other sort of post-processing.

Due to their role in the IPCC assessment (and for analysing mitigation options in line with temperature targets more generally), understanding the degree to which RCMs can reflect a range of radiative forcing, warming, heat uptake and concentration assessments simultaneously is of vital importance.
If RCMs are inherently biased in some way, this will affect the WG3 climate assessment and interpretation of the RCMs’ outputs should be adjusted accordingly.

This study’s scope, in terms of number of climate dimensions considered and number of climate models evaluated, is unique.
There have been studies with single models which choose parameter sets that match various assessments of ECS and TCR \cite{meinshausen_2009_ghgtargets,rogelj_2012_warming}.
\citeA{Smith_2018_gdrwm6} compared two models’ probabilistic outputs.

Here, in the second phase of RCMIP, we evaluate the degree to which multiple RCMs are able to synthesise Earth system knowledge within a probabilistic distribution.
We then examine the implications of differences in these probabilistic distributions for climate projections.
We extend previous probabilistic evaluation work and build on the progress made in the first phase \cite{nicholls_2020_rcmip1} and other RCM intercomparison studies \cite{Vuuren_2009_iamsclimatechange,Harmsen_2015_iamnonco2,Schwarber_2019_impulses}.
We widen the first phase’s scope both in terms of number of climate dimensions considered and the number of models evaluated.
To our knowledge, this is the most comprehensive evaluation performed to date of the ability of RCMs to capture a broad range of climate metrics and key indicators, such as those assessed in by IPCC WG1.
