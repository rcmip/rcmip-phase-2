We have found that the best performing RCMs can match our proxy assessment across a range of climate metrics.
However, no RCM matched the proxy assessment across all metrics.
At the same time, all RCMs matched the proxy assessment well for at least one metric.

Our evaluation is the first multi-model comparison of probabilistic projections from RCMs.
This exercise provides a unique insight into RCMs probabilistic parameter ensembles, specifically how they compare with a set of proxy assessed ranges, which reflect wider scientific understanding of key climate metrics, and the implications of differences in probabilistic distributions for climate projections across a range of climate variables and scenarios.

Notably, although unsurprisingly, we found that models whose probabilistic distribution were constrained to the proxy assessed ranges were better able to reflect the proxy assessed ranges.
This point is notable because it makes clear that if RCMs are to be used as integrators of knowledge, conveying multiple lines of evidence from one domain to another (e.g. IPCC WG1 to IPCC WG3), then RCMs whose probabilistic distributions have been constrained to the intended lines of evidence are likely to be the best tool.

Even amongst models which had similar levels of agreement with the proxy assessment, some divergence in future projections was observed.
Given the various model structures that the reduced complexity models employ, ranging from linearised impulse response functions to 50-layer ocean models, it is not surprising that models may diverge in scenarios that go significantly beyond the domain of the validation data.
Adding constraints on future performance i.e. extending the domain of validation data (for example based on an independent assessment of warming in a limited subset of scenarios) would likely reduce the divergence, although such extra constraints should be carefully considered given that they risk artificially narrowing projection uncertainty.

While exercises such as the one performed here can provide helpful information about where the biases may lie, they cannot provide definitive answers about what the future holds.
It is possible to make judgements about what is more reasonable based on the evaluation performed here, and to rule out clearly incorrect projections, yet it must be recognised that a definitive answer is impossible: we will not know which projections are correct until we get there, by which time it is too late for climate policy.
Hence, while it is important to continue to evaluate and improve our models to remove as many sources of error as possible, it is also important that research into decision making under uncertainty \cite<e.g.>[]{weaver_2013_decision,dittrich_2016_decision} continues to develop and be used because the uncertainty in projections will not disappear anytime soon, never in fact.
In addition, those who use RCMs for climate projections should carefully consider how they’re going to use the RCMs and how they’re going to validate them before making conclusions about the implications of their projections.

In addition, we found that many of the RCMs did not reproduce the high warming seen in CMIP6 models.
However, studies which constrain CMIP6 models based on observational constraints also exclude such high warming which suggests that the lack of high warming is due to the constraining applied to the RCMs, rather than structural differences between RCMs and CMIP6 models.
Beyond the question of temperature projections, we found that the prescribed \CO2 concentrations used in the CMIP6 SSP-based experiments are at the high-end of projections made with historically constrained carbon cycles.
Although, further investigations into carbon cycle behaviour are required to provide a clearer picture of the influence of carbon cycle uncertainties on emissions-driven projections.
Finally, we observed that a change in reference period significantly altered how well some models agreed with observations, reinforcing the need to consider more than one reference period when evaluating models.

With sufficient validations, RCMs provide a unique synthesis tool to integrate the latest scientific understanding, including its uncertainties, along the complex cause-effect chain from emissions to global-mean temperatures.
Integrating this understanding in an internally consistent RCM framework, with all the implicit cross-correlations, is our best method to inform decision-making and other scientific domains, for example the likelihood of exceeding a given global-mean temperature threshold under a specific emissions scenario.
Further developing these tools opens vast opportunities to go beyond global-mean variables and temperature changes, and to robustly represent the complex science beneath.
