In this study, the RCMs are run in a probabilistic setup, also referred to as a probabilistic distribution.
As discussed in the introduction, a probabilistic setup means that each RCM is run with an ensemble of parameter configurations.
Specifically, for a given experiment, each RCM is run multiple times, each time in a different configuration i.e. with different parameter values.
All of these different runs are then combined to form a probabilistic set of outputs.
With these probabilistic sets, we can then calculate ranges of each output variable of interest (e.g. global-mean surface temperatures).

Modelling groups use a range of techniques to derive their parameter ensembles i.e. to constrain their models (Table \ref{tab:participating_models}).
In each probabilistic run, the parameter ensemble is fixed i.e. the same set of parameter configurations will be used in each experiment.
This choice ensures that the model outputs are deterministic, rather than including a random element due to e.g. sampling parameter values from a range or probability distribution for each run.
Typically, modelling groups will also use different data to derive their parameter ensemble.
This can lead to differences in model projections which are simply based on choices made by the modelling groups and are not related to model structure or constraining technique at all.
In this study, two models (MAGICC7 and MCE-v1-2) have used a common set of target assessed ranges, i.e. benchmarks, to derive their probabilistic distributions.
For these models, we are able to rule out the choice of data as the cause of difference between these models.
Accordingly, we can more clearly identify the importance of model structure and constraining technique for future projections.

In this study, our target assessment is a `proxy assessment’, which uses assessed climate system characteristics in line with IPCC AR5 as its starting point and updates key values using more recent literature (Table \ref{tab:assessed_ranges}).
We explicitly use the name `proxy assessment’ throughout to make clear that we are not constraining to any ranges coming from the formal IPCC assessment, rather an approximation thereof.
Notably, in this study, the proxy assessment does not include any future projections.
While we examine future projections coming from the models, we do not explicitly compare them against future projections coming from another line of evidence because there is no obvious choice for such a line of evidence - apart from the `assessed ranges' of SSP scenarios that will be communicated in the forthcoming IPCC report (but are not available for this study).
As discussed in more detail in Section \ref{sub:further_discussion}, the inclusion of future projections in the proxy assessment would narrow the range of model projections but any such narrowing should be carefully considered because - depending on the types of constraints - it may lead to underestimates of uncertainty.

In order to keep the study's scope manageable, our proxy assessment focuses on climate response parameters, with the carbon cycle examined only via the TCRE.
We aim to perform a detailed analysis on carbon cycle response in the next phase of RCMIP.

\begin{table}
\caption{
    The proxy assessed ranges used in this study.
    The assessed ranges are labelled as `vll’ (very-likely lower i.e. 5\textsuperscript{th} percentile), `ll’ (likely lower, 17\textsuperscript{th} percentile), `c’ (central, 50\textsuperscript{th} percentile), `lu’ (likely upper, 83\textsuperscript{th} percentile) and `vlu’ (very-likely upper, 95\textsuperscript{th} percentile).
    Sources are described in Section \ref{sec:methods}.
}
\label{tab:assessed_ranges}
% \small
\input{assessed-ranges-table.tex}
\end{table}

We use surface air ocean blended temperatures from the HadCRUT.4.6.0.0 dataset \cite{Morice_2012_q4f}.
HadCRUT4.6.0.0 is a widely used observational data product and is representative of other observations of changes in surface air and ocean temperatures \cite{simmons_2017_obsreanalyses}.
Our key metric for evaluating RCM temperature projections is the warming between the 1961-1990 and 2000-2019 periods (using the SSP2-4.5 scenario to extend the CMIP6 historical experiment to 2019).
We choose a relatively recent period to match the increase in global observations since the 1960s.

For ocean heat content, we use the recent work of \citeA{von-schuckmann_2020_adi3jd}. We focus on the change in ocean heat content between 1971 and 2018, when the largest set of observations are available.

We use the recent assessment of \citeA{sherwood_2020_aducia} for equilibrium climate sensitivity (ECS).
ECS is defined as the equilibrium warming which occurs under a doubling of atmospheric \CO2 concentrations relative to pre-industrial concentrations.
The ECS assessment is combined with the constrained transient climate response (TCR) assessment of \citeA{tokarska_2020_sic82j}.
TCR is defined as the surface air temperature change which occurs at the time at which atmospheric \CO2 concentrations double in an experiment in which atmospheric \CO2 concentrations rise at one percent per year (a 1pctCO2 experiment).
Carbon cycle behaviour is considered only via the transient climate response to emissions (TCRE).
TCRE is defined as the ratio of surface air temperature change to cumulative \CO2 emissions at the time when atmospheric \CO2 concentrations double in a 1pctCO2 experiment.
We use the TCRE assessment from \citeA{arora_2020_cd8eij}, which is based on the latest generation of Earth System Models which have participated in CMIP6 \cite{eyring_2016_fjksdl}.
There is a potential inconsistency between our ECS, TCR and TCRE ranges, which arises because the ECS assessment comes from a study which uses multiple lines of evidence, the TCR assessment is based on a constrained set of CMIP6 models and the TCRE assessment is based on unconstrained CMIP6 Earth System Models.
We discuss the importance of this inconsistency and its consequences in Section \ref{sec:results_and_discussion}.

The other key metrics are related to effective radiative forcing \cite<ERF,>{forster_2016_erfdiagnosis}.
These values generally follow the AR5 assessment, except for aerosol, \CO2 and methane ERF.
For aerosol and \CO2 ERF, we use the more recent work of \citeA{smith_2020_sicjd2}.
For methane ERF, we increase the AR5 assessment following \citeA{Etminan_2016_gdkwbj} although we note that this increase may be offset by an updated understanding of the impact of rapid adjustments \cite{smith_2018_ucke5}.

At this point, we stress that our proxy assessed ranges are only one of a range of possible choices.
Assessing all the available literature is a demanding task that is well undertaken by the IPCC.
We do not attempt to reproduce this task here.
Instead, the key is that our proxy assessed ranges are a) reasonable and b) were available at the time of the study's inception.

Following this intercomparison consortium’s choice of proxy assessed ranges, modelling groups then had the opportunity to develop parameter ensembles which best reflected these assessed ranges.
As previously discussed, this allowed some modelling teams (although crucially not all) to use the same `constraining benchmarks’ (with a number of different techniques being employed to consider the constraining benchmarks, see Table \ref{tab:participating_models}).
We use these consistently constrained models to gain unique insights into the impact of differences in model structure and constraining techniques when RCMs are used as integrators of knowledge, free from a typical source of disagreement between the models, namely that they were constrained to reproduce different understandings of the climate.
The inclusion of results from models which were not constrained using the same benchmarks allows us to quantify the importance of constraining when using reduced complexity climate models as integrators of knowledge.

The modelling groups submitted a range of concentration-driven, emission-driven and idealized scenarios for their chosen parameter subsets (see scenario specifics below).
Subsequently, several metrics were calculated, such as TCR from the idealised \CO2-only 1pctCO2 experiment (in which atmospheric \CO2 concentrations rise at 1\% per year from pre-industrial levels).
Calculating derived metrics on each individual ensemble member ensures that all metrics are calculated from internally self-consistent model runs, which is of particular importance when the metric is based on more than one output variable from the model (e.g. TCRE, which relies on both surface air temperature change and inverse emissions of \CO2).
If we instead calculated results based on percentiles of different variables, we would not be using an internally self-consistent set.
Where modelling groups felt it was more appropriate (e.g. OSCARv3.1), they performed their own weighting of ensemble members before submitting.

The one metric which is not easily calculated from model results is ECS because it is defined at equilibrium.
Accordingly, modelling groups reported their own diagnosed ECS for each ensemble member, rather than performing experiments which would allow it to be calculated after submission had taken place.

When evaluating model performance, we are interested not only in how well a model can reproduce the best estimate, but also the range, of a given quantity.
A key part of any climate assessment is the uncertainty and it is critical that RCMs reflect the assessed likely and very likely ranges if they are to be used as integrators of knowledge.
We assess the relative difference between the model and the assessed ranges at the very likely lower (5\textsuperscript{th} percentile, also referred to as `vll’), likely lower (17\textsuperscript{th} percentile, `ll'), central (50\textsuperscript{th} percentile, `c'), likely upper (83\textsuperscript{th} percentile, `lu') and very likely upper (95\textsuperscript{th} percentile, `vlu').
Assessing deviations using relative differences allows us to quickly evaluate how models perform over a range of metrics on the same scale.

The set of scenarios that each modelling group was asked to run follow the experimental protocols of CMIP6’s ScenarioMIP \cite{oneill_2016_scenariomip}.
The SSPX-Y.Y experiments (e.g. SSP1-1.9, SSP2-4.5, SSP5-8.5) are defined in terms of concentrations of well-mixed greenhouse gases i.e. \CO2, \CH4, \N2O, hydrofluorocarbons (HFCs), perfluorocarbons (PFCs) and hydrochlorofluorocarbons (HCFCs), emissions of `aerosol precursor species emissions’ i.e. sulfur, nitrates, black carbon, organic carbon and ammonia and natural effective radiative forcing variations.
As described in \citeA{nicholls_2020_rcmip1}, where required, models may use prescribed effective radiative forcing if they do not include the required gas cycles or radiative forcing parameterisations.

The esm-SSPX-Y.Y experiments are identical to the SSPX-Y.Y experiments, except \CO2 emissions are prescribed instead of \CO2 concentrations, following the CMIP6 C4MIP protocol \cite{jones_2016_c4mip}.
Finally, we also perform esm-SSPX-Y.Y-allGHG experiments.
These are identical to the esm-SSPX-Y.Y experiments, except they are defined in terms of emissions of all well-mixed greenhouse gases, not only \CO2, rather than concentrations.
There is no equivalent of these esm-SSPX-Y.Y-allGHG experiments in the CMIP6 protocol, however it is these experiments which are of most interest to WG3, given that WG3 focuses on scenarios defined in terms of emissions alone.
We use the data sources described in \citeA{nicholls_2020_rcmip1} to specify the inputs for each of these scenarios.
The input dataset compilations, comprising emission, scenario and forcing data, as well as the protocols are archived with Zenodo \cite{nicholls_2021_rcmipprotocol} - and can contribute to scientific studies beyond this intercomparison as they largely reflect the CMIP6 experimental designs.

The protocol designed for this study requires that each RCM modelling group runs every probabilistic ensemble member once for each scenario and then submits their output for further analysis.
With nine modelling groups participating, this intercomparison project compiled a database of results containing thousands of runs for each RCM, from which we can calculate different warming, effective radiative forcing or ocean heat uptake percentiles for a wide range of scenarios.
